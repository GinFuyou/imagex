### WARNING:
**Don't take this code as example!** This software is in development state.

### Features

- crop images to given width:height with offset

### Installation
requires python 3.6 or higher
```bash 
pip install git+https://gitlab.com/GinFuyou/imagex.git
```

### How to use
#### cropping
to crop images to 800x600 with offset from top left corner by 20 pixels horizontal, 40 pixels vertical
```bash
python3 -m "imagex" /path/to/imagedir/* --crop=800:600:20:40 -o /path/to/outputdir/
```
on Windows or some terminal emulators * may not work, you can give it a dir instead or list of files
```bash
python3 -m "imagex" C:/path/to/imagedir/ --crop=800:600:20:40 -o C:/path/to/outputdir/
```

`--probe` will first show a result in system image viewer before proceeding
