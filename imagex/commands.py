#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import shutil
#from time import sleep, time

from PIL import Image

from .base import BaseImageCommand

#from multiprocessing import cpu_count

# NOTE
#from subprocess import Popen


__version__ = "0.6a2"


class Command(BaseImageCommand):
    name = 'ImageX'
    version = __version__
    #w2args = ['th', 'waifu2x.lua', '-tta', '1']

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument('-b', '--background', metavar='path', type=str,
                            help='clean this background from target')
        parser.add_argument('-c', '--crop', metavar='W:H:X:Y', type=str,
                            help='Crop images by width:height with x:y offset')
        parser.add_argument('--replace', default=False, action='store_true',
                            help='replace files if applicable')
        parser.add_argument('--resamples', action='store_true', help='guess and move resampled images')
        #parser.add_argument('-w','--waifupath',type=str, default='/home/Gin/devel/waifu2x/',
                            #help='path to waifu2x dir')
        #parser.add_argument('--poll_interval', type=int, default=10, help='poll poll interval in 0.1 sec')
        #parser.add_argument('-n', '--noise_level', type=int, choices=(0,1,2,3), default=1)
        #parser.add_argument('-m', '--method', type=str, choices=('noise', 'scale', 'noise_scale'), 
                            #default='noise_scale', help="method: (noise|scale|noise_scale) [noise_scale]")
        #parser.add_argument('-a', '--autoname', default=False, action='store_true', 
                            #help='use waifu2x auto -o output file name')
        #parser.add_argument('-t', '--threads', default=8, action='store', type=int, 
                            #help='-thread parameter to waifu2x')


    def clear_bg(self, im_path, bg_path, map_colour=(255, 0, 0)):
        img = self.open_image(im_path)
        background = self.open_image(bg_path)
        cl = Image.new(mode='RGBA', size=img.size, color=(0, 0, 0, 0))
        mp = Image.new(mode='RGB', size=img.size, color=(0, 0, 0))
        self.vprint('target: {0[0]}x{0[1]} bg: {1[0]}x{1[1]}'.format(img.size, background.size))
        t = time()
        count = 0
        for x in range(0, img.size[0]):
            for y in range(0, img.size[1]):
                pos = (x, y)
                pixel = img.getpixel(pos)
                pixel_bg = background.getpixel(pos)
                if pixel == pixel_bg:
                    mp.putpixel(pos, map_colour)
                else:
                    cl.putpixel(pos, pixel)
                count += 1
                if count % 300 == 0:
                    self.vprint("{0: >8}\r".format(count), end='', v=2)
        t = time() - t
        self.vprint('Cleared BG in {0:.2f} sec.'.format(t))
        clear_path = self.mod_filename(im_path, app='clear', ext='.png')
        map_path = self.mod_filename(im_path, app='cutmap', ext='.png')
        cl.save(clear_path, 'PNG')
        mp.save(map_path, 'PNG')

    def crop(self, img_path, width, height, x=0, y=0):
        """ Crop image to target width and height with x, y offset """
        im = self.open_image(img_path)
        box = (x, y, x+width, y+height)
        im = im.crop(box=box)
        if self.args.replace:
            output_path = img_path
        elif self.args.output:
            output_path = os.path.join(self.args.output, os.path.basename(img_path))
        else:
            name, ext = os.path.splitext(img_path)
            output_path = name + '_crop' + ext
        if self.args.probe:
            sample_image = Image.new(mode='RGB', size=im.size, color=(0, 0, 0))
            sample_image.paste(im, (0, 0))
            sample_image.thumbnail((1280, 720))
            sample_image.show()
            self.args.probe = False
            answer = ''
            while answer not in ('y', 'n', '0', '1'): 
                answer = input("\nProceed with rest of files? (y/n)\n>")
                answer = answer.lower()
            if answer in ('n', '0'):
                self.vprint("Aborted by user request.", v=0, colour='yellow')
                exit()
        im.save(output_path, 'PNG')


    def handle(self):
        #target = self.abs_path(self.args.target)
        filelist = self.make_filelist()
        samples = []
        if self.args.resamples:
            raise NotImplementedError('This command is not yet re-implemented in this version')
            for filepath in filelist:
                ext = os.path.splitext(filepath)[1]
                if ext.lower() in ('.jpeg', '.jpg', '.png', '.bmp'):
                    self.vprint(filepath, end=' ', v=1)
                else:
                    #self.vprint(filepath+" SKIP", v=2)
                    continue
                im = self.open_image(filepath)
                width, height = im.size
                self.vprint("{0}x{1}".format(width, height), end=' ', v=2)
                if width in (850, 849, 750, 580, 579, 1500):
                    samples.append(filepath)
                    self.vprint('width hit: {0}'.format(width), v=2, colour='MAGENTA')
                elif height in (1000,) and width < (height*0.95):
                    samples.append(filepath)
                    self.vprint('height and height>width hit'.format(width), v=2, colour='MAGENTA')
            self.vprint('Probably resampled: -- --', v=2)
            dest = os.path.join(os.getcwd(), '_samples')
            for filepath in samples:
                shutil.move(filepath, dest)
                self.vprint(filepath, v=1, colour='BLUE')
            self.vprint('DONE', v=1)
            return 0

        if self.args.background:
            raise NotImplementedError('This command is not yet re-implemented in this version')
            background = self.abs_path(self.args.background)
            self.clear_bg(target, background)

        if self.args.crop:
            splits = self.args.crop.split(':')
            width = int(splits[0])
            height = int(splits[1])
            arg_len = len(splits)
            x = 0
            y = 0
            if arg_len > 2:
                x = int(splits[2])
                if arg_len == 4:
                    y = int(splits[3])
            counter = 1
            for filepath in filelist:
                self.vprint(f"[{counter:03}/{self.counter}]", end=' ', v=1, colour='green')
                self.vprint(f"{filepath}", end=' ', v=1)
                self.crop(filepath, width=width, height=height, x=x, y=y)
                self.vprint('[DONE]', v=1)
                counter += 1


if __name__ == '__main__':
    Command().execute()
