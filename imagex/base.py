import argparse
import os
#from time import localtime, strftime

from colorama import Back, Fore, Style
from colorama import init as colorama_init
from PIL import Image
from PIL import __version__ as pillow_version


class BaseImageCommand:
    """
    Base of command class. (I probably should revise my code structure)
    """
    help = ''
    default_args = ''
    version = '0.1a0'
    name = 'BaseCommand'
    args = None
    start_text = '* {0.name} ver.{1.MAGENTA}{0.version}{2.RESET_ALL} started! (verbosity {0.args.verbosity})'
    Style = Style
    MAX_FILELIST_ITEMS = 300
    expanded_targets = []
    counter = 0

    def get_fore(self, colour='WHITE'):
        """ utility to colorama """
        return getattr(Fore, colour.upper(), 'WHITE')

    def create_parser(self):
        """
        Create and return the ``ArgumentParser`` which will be used to
        parse the arguments to this command.

        """
        parser = argparse.ArgumentParser(description=self.help or None)
        parser.add_argument('--version', action='version', version=self.version)
        parser.add_argument('-v', '--verbosity', action='store', dest='verbosity', default=1,
                            type=int, choices=[0, 1, 2, 3],
                            help='Verbosity level; 0=minimal output, 1=normal output, 2=verbose output, 3=very verbose output')
        parser.add_argument('--traceback', action='store_true',
                            help='Raise on CommandError exceptions')
        parser.add_argument('--no-color', action='store_true', dest='no_color', default=False,
                            help="Don't colorize the command output.")
        parser.add_argument('--filter-ext', action='store', dest='allowed_extensions',
                            type=str, default='.png,.jpeg,.jpg,.bmp',
                            help="list of allowed file extension, comma separated")
        parser.add_argument('-o', '--output', action='store',
                            type=str, default='',
                            help="output dir, default is empty for same dir as source file")
        parser.add_argument('--probe', action='store_true',
                            help="first probe result before processing all files")

        if self.default_args:
            # Keep compatibility and always accept positional arguments, like optparse when args is set
            parser.add_argument('args', nargs='*')
        self.add_arguments(parser)
        return parser

    def add_arguments(self, parser):
        """ add more args, used in inheritance """
        parser.add_argument('target', metavar='path', nargs='+', type=str,
                            help='input image to remove background')

    def clean_extensions(self):
        """ Fix input extensions with dot prefix """
        if self.args.allowed_extensions:
            cleaned_extensions = []
            self.args.allowed_extensions = self.args.allowed_extensions.replace(' ', '').split(',')
            for ext in self.args.allowed_extensions:
                if ext[0] != '.':
                    ext = f".{ext}"
                ext = ext.lower()
                if ext not in cleaned_extensions:
                    cleaned_extensions.append(ext)
            self.args.allowed_extensions = cleaned_extensions
            self.vprint(f"- allowed file extensions: { ', '.join(self.args.allowed_extensions) }", v=2)

    def validate_parameters(self):
        """ check if parameters make sense, like paths exist """
        self.clean_extensions()
        if self.args.output:
            output_path = self.args.output
            if not os.path.isdir(output_path):
                try:
                    os.makedirs(output_path)
                except OSError as exc:
                    self.vprint(f"Couldn't find or create output dir '{output_path}' ({exc})",
                                colour='red',
                                v=0)
                    exit()

        # expand * if not supported, make paths absolute
        for path in self.args.target:
            fullpath = self.abs_path(path)
            if fullpath.endswith('/*') or os.path.isdir(fullpath):
                self.expanded_targets += [os.path.join(fullpath, x) for x in os.listdir(fullpath[:-1])]
                self.vprint(f"? listdir '{fullpath}'", v=3, colour='blue')
            else:
                self.expanded_targets.append(fullpath)

    def execute(self):
        """
        Try to execute this command, performing system checks if needed (as
        controlled by attributes ``self.requires_system_checks`` and
        ``self.requires_model_validation``, except if force-skipped).
        """
        parser = self.create_parser()
        self.args = parser.parse_args()
        colorama_init()
        start_text = self.start_text.format(self, Fore, Style)
        self.vprint(start_text, v=1)
        self.validate_parameters()
        self.handle()

    def vprint(self, text, v=2, end='\n', colour=None):
        if self.args.verbosity >= v:
            if colour:
                f = self.get_fore(colour)
                text = "{0}{1}{2.RESET_ALL}".format(f, text, Style)
            print(text, end=end)

    def handle(self):
        """
        The actual logic of the command. Subclasses must implement
        this method.

        """
        raise NotImplementedError('subclasses of BasePonyCommand must provide a handle() method')

    def open_image(self, path):
        """ open file verifying images with PIL """
        try:
            im = Image.open(path)
            im.verify()
        except Exception as ex:
            self.vprint("Error: file haven't got verified: {0}".format(ex), v=0, colour='RED')
            raise RuntimeError
        else:
            self.vprint('* File verifed by Pillow v.{0}'.format(pillow_version), v=2, colour='GREEN')
            return Image.open(path)

    def mod_filename(self, path, pre='', app='', ext='', nameonly=False):
        directory, filename = os.path.split(path)
        name, old_ext = os.path.splitext(filename)
        if ext and ext[0] != '.':
            ext = '.' + ext
        elif not ext:
            ext = old_ext

        if pre and pre[-1] not in ('-', '_', ' '):
            pre += ' '
        if app and app[0] not in  ('-', '_', ' '):
            app = ' ' +app
        filename = "{pre}{name}{app}{ext}".format(name=name, pre=pre, app=app, ext=ext)
        if nameonly:
            return filename
        else:
            path = os.path.join(directory, filename)
            return path

    def abs_path(self, path):
        """ make absolute filepath """
        if path == '':
            path = os.getcwd()
        if path[0] != '/':
            cwd = os.getcwd()
            path = os.path.join(cwd, path)
            self.vprint("Relative path detected, will try '{0}' instead".format(path), v=3, colour='YELLOW')
        return path

    def filter_path(self, path):
        """ filter out target by extension """
        if self.args.allowed_extensions:
            ext = os.path.splitext(path)[1].lower()
            if ext and (ext in self.args.allowed_extensions):
                return path
            else:
                self.vprint(f"! Path '{os.path.basename(path)}' filtered out", v=3, colour='yellow')
                return None

    def make_filelist(self):
        """ make a filtered list of filepathes to work with """
        self.vprint(f"- target: {self.expanded_targets}", v=2)
        filelist = []
        for fullpath in self.expanded_targets:
            if self.filter_path(fullpath):
                self.counter += 1
                if self.counter < self.MAX_FILELIST_ITEMS:
                    self.vprint(f"[{self.counter:03}] {fullpath}", v=2)
                elif self.counter == self.MAX_FILELIST_ITEMS:
                    self.vprint(f"- Maximum filelist items display reached, further lines are hidden", v=2)
                filelist.append(fullpath)
        if self.counter:
            self.vprint(f"- {self.counter} files found, proceeding", v=1)
        else:
            self.vprint("! no valid files found, aborting", v=0, colour='red')
            raise RuntimeError('No files to work with')
        return filelist
