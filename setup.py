#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages

import codecs
import re
import os


base_path = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    # intentionally *not* adding an encoding option to open, See:
    #   https://github.com/pypa/virtualenv/issues/201#issuecomment-3145690
    return codecs.open(os.path.join(base_path, *parts), 'r').read()


def find_version(*path_parts, version_pattern="(\d+(?:\.\d+)*(a|b|rc|dev)\d*)"):
    version_data = read(*path_parts)
    pattern = r"^__version__ = [\'\"]{0}[\'\"]".format(version_pattern)
    version_match = re.search(pattern, version_data, re.M)
    if version_match:
        return version_match.group(1)
    else:
        raise RuntimeError("Unable to find version string. (tried pattern: <{0}>)".format(pattern))


setup(  name='ImageX',
        version=find_version("imagex", "commands.py"),
        description='Simple operations on images',
        author='Gin Fuyou',
        author_email='devel@doratoa.net',
        url='',
        packages= find_packages(exclude=('tests*',)),
        install_requires=['pillow', 'colorama'] )
